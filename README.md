# Legion Theme

* Tema para BlizzCMS-Plus basado en la Expansión de Legion de World of Warcraft.

# Project

* https://wow-cms.com/en/
* https://gitlab.com/WoW-CMS

# Installation

* Add the "legion" folder in /application/themes/
* Then go to the admin panel and rename the theme to "legion" save changes and you're good to go.

# Requirements

* BlizzCMS-Plus

# Author

* @Black_Tyrael

# Screenshots

![Screenshot](Screenshot.png)